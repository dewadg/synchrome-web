import { normalize } from 'normalizr'
import * as types from '../types/roleTypes'
import { ROLE_LIST_SCHEMA } from '../schema/roleSchema'

export default {
  [types.FETCH_ALL_ROLES] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_ALL_ROLES_SUCCESS] (state, data) {
    state.isFetching = false
    state.data = normalize(data, ROLE_LIST_SCHEMA)
  },

  [types.FETCH_ALL_ROLES_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  }
}
