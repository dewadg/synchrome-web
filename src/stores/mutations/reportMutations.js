import * as types from '../types/reportTypes'

export default {
  [types.FETCH_TPP_BY_AGENCY_REPORT] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_TPP_BY_AGENCY_REPORT_SUCCESS] (state) {
    state.isFetching = false
  },

  [types.FETCH_TPP_BY_AGENCY_REPORT_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  },

  [types.FETCH_TPP_BY_ASN_REPORT] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_TPP_BY_ASN_REPORT_SUCCESS] (state) {
    state.isFetching = false
  },

  [types.FETCH_TPP_BY_ASN_REPORT_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  }
}
