import { normalize } from 'normalizr'
import * as types from '../types/userTypes'
import { USER_LIST_SCHEMA } from '../schema/userSchema'

export default {
  [types.FETCH_ALL_USERS] (state) {
    state.isFetching = true
    state.error = null
  },

  [types.FETCH_ALL_USERS_SUCCESS] (state, data) {
    state.isFetching = false
    state.data = normalize(data, USER_LIST_SCHEMA)
  },

  [types.FETCH_ALL_USERS_ERROR] (state, err) {
    state.isFetching = false
    state.error = err
  },

  [types.STORE_USER] (state) {
    state.isStoring = true
    state.error = null
  },

  [types.STORE_USER_SUCCESS] (state) {
    state.isStoring = false
  },

  [types.STORE_USER_ERROR] (state, err) {
    state.isStoring = false
    state.error = err
  },

  [types.FETCH_ONE_USER] (state) {
    state.isFetchingOne = true
    state.error = null
  },

  [types.FETCH_ONE_USER_SUCCESS] (state) {
    state.isFetchingOne = false
  },

  [types.FETCH_ONE_USER_ERROR] (state, err) {
    state.isFetchingOne = false
    state.error = err
  },

  [types.UPDATE_USER] (state) {
    state.isUpdating = true
    state.error = null
  },

  [types.UPDATE_USER_SUCCESS] (state) {
    state.isUpdating = false
  },

  [types.UPDATE_USER_ERROR] (state, err) {
    state.isUpdating = false
    state.error = err
  },

  [types.DESTROY_USER] (state) {
    state.isDestroying = true
    state.error = null
  },

  [types.DESTROY_USER_SUCCESS] (state) {
    state.isDestroying = false
  },

  [types.DESTROY_USER_ERROR] (state, err) {
    state.isDestroying = false
    state.error = err
  },

  [types.SET_USER_FORM] (state, form) {
    state.form = {
      ...state.form,
      ...form
    }
  },

  [types.RESET_USER_FORM] (state) {
    state.form = {
      id: null,
      name: '',
      fullName: '',
      password: '',
      passwordConf: '',
      roleId: null
    }
  }
}
