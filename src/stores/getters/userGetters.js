import { denormalize } from 'normalizr'
import * as types from '../types/userTypes'
import { USER_LIST_SCHEMA } from '../schema/userSchema'

export default {
  [types.GET_USER_DATA]: state => denormalize(state.data.result, USER_LIST_SCHEMA, state.data.entities),
  [types.GET_USER_FORM]: state => state.form
}
