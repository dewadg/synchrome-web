import { denormalize } from 'normalizr'
import * as types from '../types/roleTypes'
import { ROLE_LIST_SCHEMA } from '../schema/roleSchema'

export default {
  [types.GET_ROLE_DATA]: state => denormalize(state.data.result, ROLE_LIST_SCHEMA, state.data.entities)
}
