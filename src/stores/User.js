import state from './states/userState'
import getters from './getters/userGetters'
import mutations from './mutations/userMutations'
import actions from './actions/userActions'

export default {
  state,
  getters,
  mutations,
  actions
}
