import state from './states/reportState'
import mutations from './mutations/reportMutations'
import actions from './actions/reportActions'

export default {
  state,
  mutations,
  actions
}
