import { userService } from '@/services'
import * as types from '../types/userTypes'

export default {
  async [types.FETCH_ALL_USERS] ({ commit }) {
    commit(types.FETCH_ALL_USERS)

    try {
      commit(types.FETCH_ALL_USERS_SUCCESS, await userService.get())
    } catch (err) {
      commit(types.FETCH_ALL_USERS_ERROR, err)
    }
  },

  async [types.STORE_USER] ({ commit }, payload) {
    commit(types.STORE_USER)

    try {
      const user = await userService.create(payload)
      commit(types.STORE_USER_SUCCESS)

      return user
    } catch (err) {
      commit(types.STORE_USER_ERROR, err)
    }
  },

  async [types.FETCH_ONE_USER] ({ commit }, id) {
    commit(types.FETCH_ONE_USER)

    try {
      const user = await userService.find(id)

      commit(types.FETCH_ONE_USER_SUCCESS)

      return user
    } catch (err) {
      commit(types.FETCH_ONE_USER_ERROR, err)
    }
  },

  async [types.UPDATE_USER] ({ commit }, { id, data }) {
    commit(types.UPDATE_USER)

    try {
      await userService.update(id, data)

      commit(types.UPDATE_USER_SUCCESS)
    } catch (err) {
      commit(types.UPDATE_USER_ERROR, err)
    }
  },

  async [types.DESTROY_USER] ({ commit }, id) {
    commit(types.DESTROY_USER)

    try {
      await userService.delete(id)

      commit(types.DESTROY_USER_SUCCESS)
    } catch (err) {
      commit(types.DESTROY_USER_ERROR, err)
    }
  }
}
