import * as types from '../types/reportTypes'
import { reportService } from '@/services'

export default {
  async [types.FETCH_TPP_BY_AGENCY_REPORT] ({ commit }, { agencyId, start, end }) {
    commit(types.FETCH_TPP_BY_AGENCY_REPORT)

    try {
      const data = await reportService.getTppByAgencyReport({ agencyId, start, end })

      commit(types.FETCH_TPP_BY_AGENCY_REPORT_SUCCESS)

      return data
    } catch (err) {
      commit(types.FETCH_TPP_BY_AGENCY_REPORT_ERROR, err)
    }
  },

  async [types.FETCH_TPP_BY_ASN_REPORT] ({ commit }, { asnId, start, end }) {
    commit(types.FETCH_TPP_BY_ASN_REPORT)

    try {
      const data = await reportService.getTppByAsnReport({ asnId, start, end })

      commit(types.FETCH_TPP_BY_ASN_REPORT_SUCCESS)

      return data
    } catch (err) {
      commit(types.FETCH_TPP_BY_ASN_REPORT_ERROR, err)
    }
  }
}
