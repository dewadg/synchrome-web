import { roleService } from '@/services'
import * as types from '../types/roleTypes'

export default {
  async [types.FETCH_ALL_ROLES] ({ commit }) {
    commit(types.FETCH_ALL_ROLES)

    try {
      commit(types.FETCH_ALL_ROLES_SUCCESS, await roleService.get())
    } catch (err) {
      commit(types.FETCH_ALL_ROLES_ERROR, err)
    }
  }
}
