import { schema } from 'normalizr'
import { ROLE_SCHEMA } from '../schema/roleSchema'

export const USER_SCHEMA = new schema.Entity('users', {
  role: ROLE_SCHEMA
})
export const USER_LIST_SCHEMA = [USER_SCHEMA]
