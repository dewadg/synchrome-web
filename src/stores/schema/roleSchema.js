import { schema } from 'normalizr'
import { ACCESS_LIST_SCHEMA } from './accessSchema'

export const ROLE_SCHEMA = new schema.Entity('roles', {
  accesses: ACCESS_LIST_SCHEMA
})
export const ROLE_LIST_SCHEMA = [ROLE_SCHEMA]
