import { schema } from 'normalizr'

export const ACCESS_SCHEMA = new schema.Entity('accesses')
export const ACCESS_LIST_SCHEMA = [ACCESS_SCHEMA]
