import state from './states/roleState'
import getters from './getters/roleGetters'
import mutations from './mutations/roleMutations'
import actions from './actions/roleActions'

export default {
  state,
  getters,
  mutations,
  actions
}
