import chainMiddlewares from '@/middlewares'
import { auth } from '@/middlewares/auth'

export default [
  {
    path: '/report',
    name: 'report',
    component: () => import('@/views/Report/AgencyTppReport'),
    beforeEnter: chainMiddlewares([
      auth
    ])
  },
  {
    path: '/report/:asnId/:start/:end',
    name: 'report.asn',
    component: () => import('@/views/Report/AsnTppReport'),
    beforeEnter: chainMiddlewares([
      auth
    ])
  }
]
