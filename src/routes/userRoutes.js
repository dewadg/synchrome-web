import chainMiddlewares from '@/middlewares'
import { auth } from '@/middlewares/auth'

export default [
  {
    path: '/users',
    name: 'users',
    component: () => import('@/views/User/UserIndex'),
    beforeEnter: chainMiddlewares([
      auth
    ])
  },
  {
    path: '/users/new',
    name: 'users.create',
    component: () => import('@/views/User/UserCreate'),
    beforeEnter: chainMiddlewares([
      auth
    ])
  },
  {
    path: '/users/:id',
    name: 'users.edit',
    component: () => import('@/views/User/UserEdit'),
    beforeEnter: chainMiddlewares([
      auth
    ])
  }
]
