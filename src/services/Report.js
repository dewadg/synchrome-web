import { removeDataNamespace } from '@/helpers/data'

export default class Rank {
  _http = null

  constructor (httpService) {
    this._http = httpService
  }

  async getTppByAgencyReport ({ agencyId, start, end }) {
    const res = await this._http.post('report/tpp-by-agency', {
      agencyId,
      start,
      end
    })

    return removeDataNamespace(res.data)
  }

  async getTppByAsnReport ({ asnId, start, end }) {
    const res = await this._http.post('report/tpp-by-asn', {
      asnId,
      start,
      end
    })

    return removeDataNamespace(res.data)
  }
}
