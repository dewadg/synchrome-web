import { removeDataNamespace } from '@/helpers/data'

export default class Role {
  _http = null

  constructor (httpService) {
    this._http = httpService
  }

  async get () {
    const res = await this._http.get('roles')

    return removeDataNamespace(res.data)
  }
}
