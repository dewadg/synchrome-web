import { removeDataNamespace } from '@/helpers/data'

export default class User {
  _http = null

  constructor (httpService) {
    this._http = httpService
  }

  async get () {
    const res = await this._http.get('users')

    return removeDataNamespace(res.data)
  }

  async create (payload) {
    const res = await this._http.post('users', payload)

    return removeDataNamespace(res.data)
  }

  async find (id) {
    const res = await this._http.get(`users/${id}`)

    return removeDataNamespace(res.data)
  }

  async update (id, payload) {
    const res = await this._http.patch(`users/${id}`, payload)

    return removeDataNamespace(res.data)
  }

  async delete (id) {
    await this._http.delete(`users/${id}`)
  }
}
