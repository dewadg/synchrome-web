import { required } from 'vuelidate/lib/validators'

export default {
  form: {
    agencyId: { required },
    start: { required },
    end: { required }
  }
}
