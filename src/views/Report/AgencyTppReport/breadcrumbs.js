export default [
  {
    text: 'Dashboard',
    to: {
      name: 'dashboard'
    },
    exact: true
  },
  {
    text: 'Laporan TPP per OPD',
    disabled: true
  }
]
