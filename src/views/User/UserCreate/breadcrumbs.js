export default [
  {
    text: 'Dashboard',
    to: {
      name: 'dashboard'
    },
    exact: true
  },
  {
    text: 'Pengguna',
    to: {
      name: 'users'
    },
    exact: true
  },
  {
    text: 'Tambah Pengguna',
    disabled: true
  }
]
