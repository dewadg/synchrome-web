import { required, requiredIf, sameAs } from 'vuelidate/lib/validators'

export default {
  userName: { required },
  userFullName: { required },
  userPassword: {
    required: requiredIf(vm => vm.editMode === false)
  },
  userPasswordConf: {
    required: requiredIf(vm => vm.editMode === false),
    sameAsUserPassword: sameAs('userPassword')
  },
  userRoleId: { required }
}
