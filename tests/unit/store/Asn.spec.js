import sinon, { assert } from 'sinon'
import faker from 'faker'
import { httpService, authService } from '@/services'
import actions from '@/stores/actions/asnActions'
import * as types from '@/stores/types/asnTypes'

let asn

describe('ASN Store', () => {
  before(async () => {
    const token = await authService.authenticate({
      name: process.env.VUE_APP_AUTH_USERNAME,
      password: process.env.VUE_APP_AUTH_PASSWORD
    })

    httpService.authData = token
  })

  it(`should dispatch ${types.FETCH_ALL_ASN} successfully`, async () => {
    const commit = sinon.spy()

    await actions[types.FETCH_ALL_ASN]({ commit })

    assert.match(commit.getCall(0).args[0], types.FETCH_ALL_ASN)
    assert.match(commit.getCall(1).args[0], types.FETCH_ALL_ASN_SUCCESS)
  })

  it(`should dispatch ${types.STORE_ASN} successfully`, async () => {
    const commit = sinon.spy()
    const payload = {
      id: 'T35T' + faker.lorem.word() + Math.random(),
      agencyId: '4.05',
      rankId: '1a',
      echelonId: '4.05',
      tppId: 1,
      workshiftId: 1,
      calendarId: 1,
      pin: faker.lorem.word(),
      name: faker.lorem.word(),
      phone: faker.phone.phoneNumber(),
      address: faker.lorem.word()
    }

    asn = await actions[types.STORE_ASN]({ commit }, payload)

    assert.match(commit.getCall(0).args[0], types.STORE_ASN)
    assert.match(commit.getCall(1).args[0], types.STORE_ASN_SUCCESS)
  })

  it(`should dispatch ${types.FETCH_ONE_ASN} successfully`, async () => {
    const commit = sinon.spy()

    await actions[types.FETCH_ONE_ASN]({ commit }, asn.id)

    assert.match(commit.getCall(0).args[0], types.FETCH_ONE_ASN)
    assert.match(commit.getCall(1).args[0], types.FETCH_ONE_ASN_SUCCESS)
  })

  it(`should dispatch ${types.UPDATE_ASN} successfully`, async () => {
    const commit = sinon.spy()
    const payload = {
      agencyId: '4.05',
      rankId: '1a',
      echelonId: '4.05',
      tppId: 1,
      workshiftId: 1,
      calendarId: 1,
      pin: faker.lorem.word(),
      name: faker.lorem.word(),
      phone: faker.phone.phoneNumber(),
      address: faker.lorem.word()
    }

    await actions[types.UPDATE_ASN]({ commit }, {
      id: asn.id,
      data: payload
    })

    assert.match(commit.getCall(0).args[0], types.UPDATE_ASN)
    assert.match(commit.getCall(1).args[0], types.UPDATE_ASN_SUCCESS)
  })

  it(`should dispatch ${types.DESTROY_ASN} successfully`, async () => {
    const commit = sinon.spy()

    await actions[types.DESTROY_ASN]({ commit }, asn.id)

    assert.match(commit.getCall(0).args[0], types.DESTROY_ASN)
    assert.match(commit.getCall(1).args[0], types.DESTROY_ASN_SUCCESS)
  })
})
