import sinon, { assert } from 'sinon'
import faker from 'faker'
import { httpService, authService } from '@/services'
import actions from '@/stores/actions/userActions'
import * as types from '@/stores/types/userTypes'

let user

describe('User Store', () => {
  before(async () => {
    const token = await authService.authenticate({
      name: process.env.VUE_APP_AUTH_USERNAME,
      password: process.env.VUE_APP_AUTH_PASSWORD
    })

    httpService.authData = token
  })

  it(`should dispatch ${types.FETCH_ALL_USERS} successfully`, async () => {
    const commit = sinon.spy()

    await actions[types.FETCH_ALL_USERS]({ commit })

    assert.match(commit.getCall(0).args[0], types.FETCH_ALL_USERS)
    assert.match(commit.getCall(1).args[0], types.FETCH_ALL_USERS_SUCCESS)
  })

  it(`should dispatch ${types.STORE_USER} successfully`, async () => {
    const commit = sinon.spy()
    const payload = {
      name: faker.lorem.word(),
      fullName: faker.lorem.word(),
      password: 'test',
      passwordConf: 'test',
      roleId: 1
    }

    user = await actions[types.STORE_USER]({ commit }, payload)

    assert.match(commit.getCall(0).args[0], types.STORE_USER)
    assert.match(commit.getCall(1).args[0], types.STORE_USER_SUCCESS)
  })

  it(`should dispatch ${types.FETCH_ONE_USER} successfully`, async () => {
    const commit = sinon.spy()

    user = await actions[types.FETCH_ONE_USER]({ commit }, user.id)

    assert.match(commit.getCall(0).args[0], types.FETCH_ONE_USER)
    assert.match(commit.getCall(1).args[0], types.FETCH_ONE_USER_SUCCESS)
  })

  it(`should dispatch ${types.UPDATE_USER} successfully`, async () => {
    const commit = sinon.spy()
    const payload = {
      name: faker.lorem.word(),
      fullName: faker.lorem.word(),
      roleId: 1
    }

    await actions[types.UPDATE_USER]({ commit }, {
      id: user.id,
      data: payload
    })

    assert.match(commit.getCall(0).args[0], types.UPDATE_USER)
    assert.match(commit.getCall(1).args[0], types.UPDATE_USER_SUCCESS)
  })

  it(`should dispatch ${types.DESTROY_USER} successfully`, async () => {
    const commit = sinon.spy()

    await actions[types.DESTROY_USER]({ commit }, user.id)

    assert.match(commit.getCall(0).args[0], types.DESTROY_USER)
    assert.match(commit.getCall(1).args[0], types.DESTROY_USER_SUCCESS)
  })
})
